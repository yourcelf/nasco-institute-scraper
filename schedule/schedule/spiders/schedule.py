from scrapy.spider import BaseSpider
from scrapy.http import Request
from scrapy.selector import Selector
from ..items import CourseItem

class ScheduleSpider(BaseSpider):
    name = "schedule"
    allowed_domains = ["www.nasco.coop"]
    start_urls = ["https://www.nasco.coop/institute/schedule"]

    def parse(self, response):
        sel = Selector(response)
        blocks = sel.xpath("//a[contains(@href, 'courses/block')]")
        for block in blocks:
            url = block.xpath("./@href").extract()[0]
            request = Request(url, callback=self.parse_block)
            yield request

    def parse_block(self, response):
        sel = Selector(response)

        times = u"".join(
            sel.xpath("//div[contains(@class, 'view-header')]//text()").extract()
        ).strip()
        block = u"".join(sel.xpath("//h1[@id='page-title']//text()").extract()).strip()
        courses = sel.xpath("//article[contains(@class, 'course-description')]")
        items = []
        for course in courses:
            link = course.xpath(".//h2/a[contains(@href, '/courses')]")
            item = CourseItem()
            item['times'] = times
            item['block'] = block
            item['title'] = u"".join(link.xpath("./text()").extract())
            item['url'] = "https://www.nasco.coop{}".format(
                u"".join(link.xpath("./@href").extract())
            )
            item['presenters'] = u"".join(
                course.xpath(
                    ".//div[contains(@class, 'field-name-field-presenter')]//text()"
                ).extract()
                ).replace(u"\xa0", u" ").replace("Presenter(s): ", "").strip()
            items.append(item)
        return items

